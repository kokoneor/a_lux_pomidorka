<?php
return [
    'adminEmail' => 'robot@pomidorka.kz',
    'supportEmail' => 'robot@pomidorka.kz',
    'senderEmail' => 'robot@pomidorka.kz',
    'senderName' => 'robot@pomidorka.kz',
    'user.passwordResetTokenExpire' => 3600,
];
