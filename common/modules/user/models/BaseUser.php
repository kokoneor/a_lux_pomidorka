<?php

namespace common\modules\user\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * BaseUser model
 *
 * @property integer $id
 * @property string  $phone
 * @property string  $email
 * @property string  $auth_key
 * @property string  $password_hash
 * @property string  $token
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class BaseUser extends ActiveRecord implements IdentityInterface
{
    const ROLE_GUEST     = 0;
    const ROLE_USER      = 1;
    const ROLE_OPERATOR  = 4;
    const ROLE_MANAGER   = 8;
    const ROLE_ADMIN     = 16;
    const ROLE_DEVELOPER = 32;

    /**
     * User just register your account
     */
    const STATUS_READY = 0;
    /**
     * User confirm your account
     */
    const STATUS_ACTIVE = 1;
    /**
     * User has been deleted
     */
    const STATUS_DELETED = -1;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_READY   => 'ready',
            self::STATUS_ACTIVE  => 'active',
            self::STATUS_DELETED => 'deleted',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_READY      => 'Зарегистрированный',
            self::STATUS_ACTIVE     => 'Активный',
            self::STATUS_DELETED    => 'Удаленный',
        ];
    }

    /**
     * @return array
     */
    public static function roles()
    {
        return [
            self::ROLE_GUEST        => 'guest',
            self::ROLE_USER         => 'user',
            self::ROLE_OPERATOR     => 'operator',
            self::ROLE_MANAGER      => 'manager',
            self::ROLE_ADMIN        => 'admin',
            self::ROLE_DEVELOPER    => 'developer',
        ];
    }

    /**
     * @return array
     */
    public static function roleDescription()
    {
        return [
            self::ROLE_GUEST        => 'Гость',
            self::ROLE_USER         => 'Пользователь',
            self::ROLE_OPERATOR     => 'Оператор',
            self::ROLE_MANAGER      => 'Менеджер',
            self::ROLE_ADMIN        => 'Администратор',
            self::ROLE_DEVELOPER    => 'Разработчик',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
//        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by phone
     * @param string $phone
     * @return static|null
     */
    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by token
     * @param $token
     * @return static|null
     */
    public static function findByToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne(['token' => $token]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'token' => $token,
//            'status' => self::STATUS_READY
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generateToken()
    {
        $this->token = \Yii::$app->security->generateRandomString(100).'_'.time();
    }

    /**
     * Removes password reset token
     */
    public function removeToken()
    {
        $this->token = null;
    }
}
