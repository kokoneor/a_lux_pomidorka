<?php

namespace api\controllers;

use api\models\ResetPasswordForm;
use yii\db\Exception;
use yii\rest\Controller;
use yii\web\HttpException;

class PasswordResetController extends Controller
{
    public function actionIndex()
    {
        @header('Content-Type: application/json; charset=utf-8');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }

        $model = new ResetPasswordForm();

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post(), '')) {

            try {
                if ($reset  = $model->resetPassword()) {
                    $response   = [
                        'message'   => 'На Вашу почту отправлено новый пароль!',
                    ];
                    return $response;
                } else {
                    \Yii::$app->response->setStatusCode(422);
                    $response   = [
                        'message'   => 'Пользователь с такой почтой или номером телефона не существует!',
                    ];
                    return $response;
                }

            } catch (\Exception $e) {
                throw new HttpException(403, $e->getMessage());
            }
        }else{
            throw new HttpException(400, 'Тут POST запрос');
        }
    }

    public function actionMobile($login)
    {
        $model = new ResetPasswordForm();

        try {
            if ($reset  = $model->resetPasswordMobile($login)) {
                $response   = [
                    'message'   => 'На Вашу почту отправлено новый пароль!',
                ];
                return $response;
            } else {
                $errors = $model->firstErrors;
                throw new Exception(reset($errors));
            }

        } catch (\Exception $e) {
            throw new HttpException(403,'Пользователь с такой почтой или номером телефона не существует');
        }
    }
}