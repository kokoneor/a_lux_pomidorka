<?php

namespace api\controllers;

use common\models\Catalog;
use common\models\Product;
use yii\rest\Controller;

class CatalogController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $catalogs   = $this->getCatalog();

        return $catalogs;
    }

    public function actionMain()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response   = Catalog::getCatalogMain();
        return $response;
    }

    public function actionView($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response   = Catalog::getCatalogProducts($id);

        return $response;
    }

    public function getCatalog()
    {
        $categories_arr = [];
        $countProduct   = 0;
        $catalogs   = Catalog::find()->where(['status' => 1])->orderBy(['sort' => SORT_ASC])->all();

        foreach ($catalogs as $catalog){
            $products   = Product::findAll(['category_id' => $catalog->id]);
            $products_arr   = [];
            foreach ($products as $product){
                $products_arr[] = [
                    'id'            => $product->id,
                    'name'          => $product->name,
                    'content'       => $product->content,
                    'price'         => Product::priceDetail($product->id),
                    'image'         => $product->image
                        ? \Yii::$app->request->hostInfo . $product->getImage()
                        : \Yii::$app->request->hostInfo. '/backend/web/no-image.png',
                ];
                $countProduct++;
            }

            $categories_arr[]   = [
                'id'            => $catalog->id,
                'name'          => $catalog->name,
                'products'      => $products_arr,
            ];
        }

        $response   = [
            'catalog'   => $categories_arr,
            'total'     => $countProduct,
        ];

        return $response;
    }

    public function actionFilter($catalog)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return 123;
    }

}