<?php

namespace api\controllers;

use common\models\Address;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class DeliveryAddressController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return Address::getAll();
    }

    public function actionView($id)
    {
        return Address::getOne($id);
    }

}
