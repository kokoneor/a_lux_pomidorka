<?php

namespace api\controllers;

use common\models\Banner;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class BannerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->getAll();
    }

    public function actionView($id)
    {
        return $this->getOne($id);
    }

    protected function getAll()
    {
        $banners    = [];
        $model      = Banner::activeBanner();

        foreach ($model as $value){
            $banners[]   = [
                'id'        => $value->id,
                'title'     => $value->title,
                'link'      => $value->link,
                'delay'     => $value->delay,
                'image'     => $value->image
                    ? \Yii::$app->request->hostInfo .$value->getImage()
                    : \Yii::$app->request->hostInfo. '/backend/web/no-image.png',
            ];
        }

        return $banners;
    }

    protected function getOne($id)
    {
        $model  = Banner::findOne(['id' => $id]);
        $banner  = [
            'id'        => $model->id,
            'title'     => $model->title,
            'link'      => $model->link,
            'delay'     => $model->delay,
            'image'     => $model->image
                ? \Yii::$app->request->hostInfo .$model->getImage()
                : \Yii::$app->request->hostInfo. '/backend/web/no-image.png',
        ];

        return $banner;
    }
}