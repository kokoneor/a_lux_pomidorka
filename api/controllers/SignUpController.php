<?php

namespace api\controllers;

use api\models\SignupForm;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\HttpException;

class SignUpController extends Controller
{
    /**
     * Signs user up.
     *
     * @return mixed
     * @throws
     */
    public function actionIndex()
    {
        $model  = new SignupForm();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post(), '')){

            try {
                if ($client = $model->signup()) {
                    $user   = $model->dataUser($client);
                    $response   = [
                        'message'   => \Yii::t('main-message', 'Благодарим Вас за регистрацию!'),
                        'token'     => $client['token'],
                        'user'      => $user,
                    ];
                    return $response;
                } else {
                    $errors = $model->firstErrors;

                    \Yii::$app->response->setStatusCode(422);
                    throw new Exception(reset($errors));
                }
            }catch (\Exception $e){
                throw new HttpException(403, $e->getMessage());
            }
        }
    }
}