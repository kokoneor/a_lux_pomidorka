<?php

namespace api\controllers;

use common\models\ProductDetails;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class BasketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $products   = [];

        $array = $_GET['array'];
        $details    = ProductDetails::find()->where(['in', 'id', $array])->all();

        $total              = 0;
        $total_count        = 0;

        foreach ($details as $detail) {
            $products[]     = [
                'id'                    => $detail->product->id,
                'detail_id'             => $detail->id,
                'type_id'               => $detail->type_id,
                'type_name'             => $detail->type->name,
                'attribute_id'          => $detail->attribute_id,
                'attribute_name'        => $detail->attribute0->name,
                'image'                 => $detail->product->image
                    ? \Yii::$app->request->hostInfo . $detail->product->getImage()
                    : \Yii::$app->request->hostInfo . '/backend/web/no-image.png',

                'name'                  => $detail->product->name,
                'description'           => $detail->product->content,
                'quantity'              => 1,
                'price'                 => $detail->price
            ];

            $total              += $detail->price;
            $total_count        += $detail;
        }

        $response     = [
            'total_price'   => $total,
            'total_count'   => $total_count,
            'products'      => array_values($products),
        ];

        return $response;
    }
}
