<?php

namespace api\controllers;

use common\models\Orders;
use yii\rest\Controller;
use yii\web\HttpException;

class PayController extends Controller
{
    const ID_ORGANIZATION   = 530986;
    const SECRET_KEY        = 'SIRbLYlkwZPM4NxI';

    public function actionIndex($id)
    {
        $order    = Orders::findOne(['id' => $id]);

        $request = [
            'pg_merchant_id'            => self::ID_ORGANIZATION,
            'pg_amount'                 => $order->sum,
            'pg_salt'                   => 'te2Q95ngY63A9',
            'pg_order_id'               => $order->id,
            'pg_description'            => 'Оплата заказа',
            'pg_user_phone'             => $order->user->phone,
            'pg_user_contact_email'     => $order->user->email,
            'pg_result_url'             => 'https://pomidorka.kz/api/pay/result',
            'pg_success_url'            => 'https://pomidorka.kz/api/pay/success',
            'pg_failure_url'            => 'https://pomidorka.kz/api/pay/failure'
        ];

//        $request['pg_testing_mode'] = 1; //add this parameter to request for testing payments

        ksort($request);
        array_unshift($request, 'payment.php');
        array_push($request, self::SECRET_KEY);


        $request['pg_sig'] = md5(implode(';', $request));

        unset($request[0], $request[1]);

        $query = http_build_query($request);

        header('Location:https://api.paybox.money/payment.php?'.$query);
        die;
    }

    public function actionResult()
    {
        if($_GET['pg_result'] == 1){
            $order = Orders::findOne(['id' => $_GET['pg_order_id']]);
            $order->statusPay   = 1;
            $order->save();

        }else{
            $data['error'] = 'Ошибка оплаты!';
            echo json_encode($data);
        }
        die;
    }

    public function actionSuccess()
    {
        try {
            $order = Orders::findOne(['id' => $_GET['pg_order_id']]);
            $order->statusPay = 1;

            if($order->save()){
                \Yii::$app->session->setFlash('success', 'Вы успешно совершили оплату! Скоро менеджеры свяжутся с Вами!');

                header("Location:https://pomidorka.kz");
                die();
//                $this->redirect('/');
            }

        }catch (\Exception $e){
            throw new HttpException(500, $e->getMessage());
        }

//        return $this->redirect('/');
    }

    public function actionFailure()
    {
        \Yii::$app->session->setFlash('error', 'Оплата не прошла! Попробуйте позднее!');
        header("Location:https://pomidorka.kz");
        die();
//        $this->redirect('/');
    }

    public function actionTest($id)
    {
        $order    = Orders::findOne(['id' => $id]);

        $request = [
            'pg_merchant_id'            => self::ID_ORGANIZATION,
            'pg_amount'                 => $order->sum,
            'pg_salt'                   => 'te2Q95ngY63A9',
            'pg_order_id'               => $order->id,
            'pg_description'            => 'Оплата заказа',
            'pg_user_phone'             => $order->user->phone,
            'pg_user_contact_email'     => $order->user->email,
            'pg_result_url'             => 'http://pomidorka.loc/api/pay/result',
            'pg_success_url'            => 'http://pomidorka.loc/api/pay/success',
            'pg_failure_url'            => 'http://pomidorka.loc/api/pay/failure'
        ];

        $request['pg_testing_mode'] = 1; //add this parameter to request for testing payments

        ksort($request);
        array_unshift($request, 'payment.php');
        array_push($request, self::SECRET_KEY);


        $request['pg_sig'] = md5(implode(';', $request));

        unset($request[0], $request[1]);

        $query = http_build_query($request);

        header('Location:https://api.paybox.money/payment.php?'.$query);
        die;
    }
}
