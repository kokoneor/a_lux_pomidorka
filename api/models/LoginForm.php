<?php

namespace api\models;

use common\models\Token;
use common\models\User;
use common\models\UserAddress;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $phone;
    public $password;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['phone', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone'             => 'Номер телефона',
            'password'          => 'Пароль',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильный логин или пароль.');
            }
        }
    }

    /**
     * @return mixed|null
     * @throws
     */
    public function auth()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->token = \Yii::$app->security->generateRandomString(100);
            $user->save();

            return $user;
        } else {
            return null;
        }
    }

    /**
     * @param  $phone
     * @param  $password
     * @return mixed|null
     * @throws
     */
    public function authMobile($phone, $password)
    {
        $this->phone    = $phone;
        $this->password = $password;

        if ($this->validate()) {
            $user = $this->getUser();
            $user->token = \Yii::$app->security->generateRandomString(100);
            $user->save();

            return $user;
        } else {
            return null;
        }
    }

    public function dataUser($model)
    {
        $userAddress    = UserAddress::find()->where(['user_id' => $model->id])->orderBy(['id' => SORT_ASC])->one();

        $user   = [
            'id'            => $model->id,
            'username'      => $model->client->username,
            'token'         => $model->token,
            'phone'         => $model->phone,
            'email'         => $model->email,
            'birthday'      => $model->client->birthday,
            'address'       => $userAddress ? $userAddress->address : '',
            'apartment'     => $userAddress ? $userAddress->apartment : '',
            'storey'        => $userAddress ? $userAddress->storey : '',
            'porch'         => $userAddress ? $userAddress->porch : '',
            'intercom'      => $userAddress ? $userAddress->intercom : '',
        ];

        return $user;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            if(strripos($this->phone, '@')){
                $this->_user = User::findByEmail($this->phone);
            }else {
                $this->_user = User::findByPhone($this->validatePhone($this->phone));
            }
        }

        return $this->_user;
    }

    public function validatePhone($phone)
    {
        if(!($phone == null)){
            $response   = preg_replace("/[^0-9]/", "", $phone);

            return $response;
        }

        return null;
    }
}
