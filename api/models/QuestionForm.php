<?php

namespace api\models;

use common\models\Feedback;
use common\models\QuestionRequest;
use common\models\User;
use yii\base\Model;

class QuestionForm extends Model
{
    public $username;
    public $email;
    public $phone;
    public $theme;
    public $message;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username'      => 'Имя/Фамилия',
            'email'         => 'Email',
            'phone'         => 'Телефон',
            'theme'         => 'Тема обращения',
            'message'       => 'Сообщение',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'phone'], 'required'],
            [['message'], 'string'],
//            [['email'], 'email'],
            [['username', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * Create new request
     * @return mixed
     */
    public function create()
    {
        if ($this->validate()) {
            $feedback           = new QuestionRequest();
            $feedback->username = $this->username;
            $feedback->phone    = $this->phone;
            $feedback->message  = $this->message;

            if($feedback->save()){
                return  true;
            }
        }

        return false;
    }
}