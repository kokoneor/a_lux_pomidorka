<?php

namespace api\models;

use common\models\OrderedProducts;
use common\models\Orders;
use common\models\PriceMinimum;
use yii\base\Model;

class OrderForm extends Model
{
    public $username;
    public $status;
    public $address;
    public $email;
    public $phone;
    public $message;
    public $sum;
    public $apartment;
    public $storey;
    public $porch;
    public $intercom;
    public $amountMoney;
    public $statusPay;
    public $typePay;
    public $typeDelivery;
    public $address_id;

    public $orderProducts;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'username'      => 'ФИО',
            'address'       => 'Адрес',
            'email'         => 'Электронная почта',
            'phone'         => 'Телефон',
            'message'       => 'Комментарий',
            'sum'           => 'Сумма заказа',
            'apartment'     => 'Квартира',
            'storey'        => 'Этаж',
            'porch'         => 'Подъезд',
            'intercom'      => 'Домофон',
            'amountMoney'   => 'Сумма наличных',
            'statusPay'     => 'Статус оплаты',
            'typePay'       => 'Тип оплаты',
            'typeDelivery'  => 'Тип доставки',
            'address_id'    => 'Адрес самовывоза',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['sum', 'priceMinimum'],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['status', 'typeDelivery', 'address_id'], 'integer'],

            [['address', 'typeDelivery'], 'required'],
            [['address', 'apartment', 'email', 'phone'], 'string', 'max' => 128],

            [['storey', 'porch', 'intercom'], 'string', 'max' => 128],

            [['statusPay', 'typePay', 'amountMoney', 'sum'], 'integer'],

            [['message'], 'string'],


        ];
    }

    /**
     * Create new order
     * @return mixed
     * @throws
     */
    public function create()
    {
        $this->orderProducts    = \Yii::$app->request->post()['orderProducts'];
        if ($this->validate()) {

            $order                      = new Orders();
            $order->order_id            = time();
            $order->user_id             = \Yii::$app->user->identity->id ? \Yii::$app->user->identity->id : null;
            $order->username            = $this->username;
            $order->sum                 = $this->sum;
            $order->status              = 1;
            $order->address             = $this->address;
            $order->email               = $this->email;
            $order->phone               = $this->phone;
            $order->message             = $this->message;

            $order->apartment           = $this->apartment;
            $order->storey              = $this->storey;
            $order->porch               = $this->porch;
            $order->intercom            = $this->intercom;
            $order->amountMoney         = $this->amountMoney;
            $order->typePay             = $this->typePay;
            $order->typeDelivery        = $this->typeDelivery;
            $order->address_id          = $this->address_id;
            $order->statusPay           = 0;

            if($order->save()){
                foreach($this->orderProducts as $item){
                    $orderedProducts                = new OrderedProducts();
                    $orderedProducts->order_id      = $order->id;
                    $orderedProducts->product_id    = $item['id'];
                    $orderedProducts->detail_id     = $item['detail_id'];
                    $orderedProducts->count         = $item['quantity'];

                    $orderedProducts->save();
                }

                $order->orderEmail($order->id);
                $order->orderEmailAdmin($order->id);

            }

            return $order;
        }

        return false;
    }


    public function priceMinimum($attribute, $params)
    {
        $priceMinimum   = PriceMinimum::find()->orderBy(['id' => SORT_ASC])->one();
        if($priceMinimum->price >= $this->sum){
            $this->addError($attribute, "$priceMinimum->content");
        }
    }

    /**
     * Update new order
     * @param $user_id
     * @param $order_id
     * @return mixed
     */
    public function orderUpdate($user_id, $order_id)
    {
        $order                      = Orders::findOne(['id' => $order_id]);
        $order->user_id             = $user_id;
        $order->save();

        return $order;
    }
}