<?php

namespace api\models;

use common\models\Client;
use common\models\User;
use common\models\UserAddress;
use yii\base\Model;
use yii\web\HttpException;

class UserAddressForm extends Model
{
    public $id;

    public $status;
    public $address;
    public $apartment;
    public $storey;
    public $porch;
    public $intercom;
    public $info;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'address'       => 'Адрес доставки',
            'apartment'     => 'Квартира',
            'storey'        => 'Этаж',
            'porch'         => 'Подъезд',
            'intercom'      => 'Домофон',
            'info'          => 'Информация',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'apartment'], 'required'],

            [['status', 'id'], 'integer'],

            ['id', 'validateId'],

            [['info'], 'string'],

            [['address', 'apartment', 'storey', 'porch', 'intercom'], 'string', 'max' => 255],

        ];
    }

    /**
     * Create address user
     * @return mixed
     * @throws
     */
    public function createAddress()
    {
        $user           = $this->getUser();

        if ($this->validate() && $user !== null) {
            $userAddress                = UserAddress::findOne(['user_id' => $user->id]);
            if(!$userAddress){
                $userAddress            = new UserAddress();
            }

            $userAddress->address       = $this->address;
            $userAddress->apartment     = $this->apartment;
            $userAddress->storey        = $this->storey;
            $userAddress->porch         = $this->porch;
            $userAddress->intercom      = $this->intercom;
            $userAddress->status        = $this->status;


            if($userAddress->save()){
                return $userAddress;
            }
        }

        return false;
    }

    /**
     * Update address user
     * @return mixed
     * @throws
     */
    public function updateAddress()
    {
        $user           = $this->getUser();
        $userAddress    = UserAddress::findOne(['user_id' => $user->id, 'id' => $this->id]);


        if ($this->validate() && $user !== null && $userAddress) {

            $userAddress->address       = $this->address;
            $userAddress->apartment     = $this->apartment;
            $userAddress->storey        = $this->storey;
            $userAddress->porch         = $this->porch;
            $userAddress->intercom      = $this->intercom;
            $userAddress->status        = $this->status;


            if($userAddress->save()){
                return $userAddress;
            }
        }

        return false;
    }

    /**
     * Validates the id.
     * This method serves as the inline validation for id.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateId($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userAddress = UserAddress::findOne(['id' => $this->id]);

            if (!$userAddress) {
                $this->addError($attribute, 'Такого адреса не существует');
            }
        }
    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['id' => \Yii::$app->user->identity->id]);
            return $this->_user;
        }
        return null;
    }

    public function dataUser($model)
    {
        $userAddress    = UserAddress::find()->where(['user_id' => $model->user_id])->orderBy(['id' => SORT_ASC])->one();

        return $userAddress;
    }
}