<?php

namespace api\models;

use common\models\OrderedProducts;
use common\models\Orders;
use common\models\PriceMinimum;
use common\models\User;
use yii\base\Model;

class OrderMobileForm extends Model
{
    public $token;
    public $username;
    public $status;
    public $address;
    public $email;
    public $phone;
    public $message;
    public $sum;
    public $apartment;
    public $storey;
    public $porch;
    public $intercom;
    public $amountMoney;
    public $statusPay;
    public $typePay;
    public $typeDelivery;
    public $address_id;

    public $orderProducts;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'username'      => 'ФИО',
            'address'       => 'Адрес',
            'email'         => 'Электронная почта',
            'phone'         => 'Телефон',
            'message'       => 'Комментарий',
            'sum'           => 'Сумма заказа',
            'apartment'     => 'Квартира',
            'storey'        => 'Этаж',
            'porch'         => 'Подъезд',
            'intercom'      => 'Домофон',
            'amountMoney'   => 'Сумма наличных',
            'statusPay'     => 'Статус оплаты',
            'typePay'       => 'Тип оплаты',
            'typeDelivery'  => 'Тип доставки',
            'address_id'    => 'Адрес самовывоза',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token'], 'required'],
            ['token', 'validateToken'],

            ['sum', 'priceMinimum'],
            [['status', 'typeDelivery', 'address_id'], 'integer'],

            [['address', 'typeDelivery'], 'required'],
            [['address', 'email', 'phone'], 'string', 'max' => 128],

            [['apartment', 'storey', 'porch', 'intercom'], 'integer'],

            [['statusPay', 'typePay', 'amountMoney', 'sum'], 'integer'],

            [['message'], 'string'],


        ];
    }

    /**
     * Validates the token.
     * This method serves as the inline validation for token.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateToken($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::findOne(['token' => $this->token]);

            if (!$user) {
                $this->addError($attribute, 'Токен параметр неправильный');
            }
        }
    }

    /**
     * Create new order
     * @return mixed
     */
    public function create()
    {
        $this->orderProducts    = \Yii::$app->request->post()['orderProducts'];
        $user       = User::findOne(['token' => $this->token]);

        if ($this->validate()) {

            $order                      = new Orders();
            $order->order_id            = time();
            $order->user_id             = $user->id;
            $order->username            = $user->client->username;
            $order->sum                 = $this->sum;
            $order->status              = 1;
            $order->address             = $this->address;
            $order->email               = $user->email;
            $order->phone               = $user->phone;
            $order->message             = $this->message;

            $order->apartment           = $this->apartment;
            $order->storey              = $this->storey;
            $order->porch               = $this->porch;
            $order->intercom            = $this->intercom;
            $order->amountMoney         = $this->amountMoney;
            $order->statusPay           = $this->statusPay;
            $order->typePay             = $this->typePay;
            $order->typeDelivery        = $this->typeDelivery;
            $order->address_id          = $this->address_id;
            $order->statusPay           = 0;

            if($order->save(false)){
                if(!empty($this->orderProducts)) {
                    foreach ($this->orderProducts as $item) {
                        $orderedProducts = new OrderedProducts();
                        $orderedProducts->order_id = $order->id;
                        $orderedProducts->product_id = $item['product'];
                        $orderedProducts->detail_id = $item['detail'];
                        $orderedProducts->count = $item['count'];

                        $orderedProducts->save(false);
                    }
                }
                return $order;
            }

        }

        return false;
    }

    public function priceMinimum($attribute, $params)
    {
        $priceMinimum   = PriceMinimum::find()->orderBy(['id' => SORT_ASC])->one();
        if($priceMinimum->price >= $this->sum){
            $this->addError($attribute, "$priceMinimum->content");
        }
    }

    /**
     * Update new order
     * @param $user_id
     * @param $order_id
     * @return mixed
     */
    public function orderUpdate($user_id, $order_id)
    {
        $order                      = Orders::findOne(['id' => $order_id]);
        $order->user_id             = $user_id;
        $order->save();

        return $order;
    }
}