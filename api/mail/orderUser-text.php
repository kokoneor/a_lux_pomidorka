<?php

use yii\helpers\Html;
use common\models\Orders;
use common\models\OrderedProducts;

/* @var $this yii\web\View */
/* @var $order Orders */
/* @var $orderedProducts OrderedProducts */

$host = Yii::$app->request->hostInfo;

?>
<?= $order->username; ?>, благодарим Вас за оформление заказа на нашем сайте Pomidorka.kz

Ниже, предоставляем Вам информацию о заказах:

<? foreach ($orderedProducts as $item): ?>
    Наименование <?= $item->product->name; ?>
    Цена за ед.  <?= $item->detail->price; ?> тг
    Кол-во       <?= $item->count; ?>
    Сумма        <?= $item->getSumCount($item->detail->price, $item->count); ?> тг

<? endforeach; ?>


Итого товара на сумму: <?= $order->sum ?> тг


С уважением администрация Pomidorka.kz
