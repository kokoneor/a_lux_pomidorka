<?php

use yii\helpers\Html;
use common\models\Orders;
use common\models\OrderedProducts;

/* @var $this yii\web\View */
/* @var $order Orders */
/* @var $orderedProducts OrderedProducts */

$host = Yii::$app->request->hostInfo;

?>
Оформлен новый заказ на сайте Pomidorka.kz

Ниже, предоставляем Вам информацию о заказах:

ФИО клиента: <?= $order->username ?>
Телефон клиента: <?= $order->phone ?>
Email клиента: <?= $order->email ?>
Адрес клиента: <?= $order->address ?>
Комментарий к заказу: <?= $order->message ?>

<? foreach ($orderedProducts as $item): ?>
    Наименование <?= $item->product->name; ?>
    Цена за ед.  <?= $item->detail->price; ?> тг
    Кол-во       <?= $item->count; ?>
    Сумма        <?= $item->getSumCount($item->detail->price, $item->count); ?> тг

<? endforeach; ?>


Итого товара на сумму: <?= $order->sum ?> тг


С уважением администрация Pomidorka
