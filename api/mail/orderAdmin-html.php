<?php

use common\models\Logo;
use yii\helpers\Html;
use common\models\Orders;
use common\models\OrderedProducts;

/* @var $this yii\web\View */
/* @var $order Orders */
/* @var $orderedProducts OrderedProducts */

$host = Yii::$app->request->hostInfo;
$logoHeader                 = Logo::getHeader();

?>
<div style="position:relative;background-color:#F8F8F8;padding:3rem .5rem;">
    <div class="logo-mail" style="text-align: center">
        <a href="/" style="width:50%;">
            <img src="<?= $logoHeader->getImage(); ?>" alt="">
        </a>
    </div>
    <!--  -->

    <div style="text-align: center;">
        <p>
            <b>Оформлен новый заказ на сайте</b>
            <b><a href="<?= $host; ?>" style="color:#fe3300;">Pomidorka.kz</a></b>
        </p>
        <p>Ниже, предоставляем Вам информацию о заказах: </p>
    </div>
    <br>
    <p>ФИО клиента: <?= $order->username ?></p><br>
    <p>Телефон клиента: <?= $order->phone ?></p><br>
    <p>Email клиента: <?= $order->email ?></p><br>
    <p>Адрес клиента: <?= $order->address ?></p><br>
    <p>Комментарий к заказу: <?= $order->message ?></p>
    <br>
    <table class="table table-striped table-bordered table-mail"
           style="width:100%;font-size:14px;border-bottom:1px dashed silver;">
        <thead>
        <tr style="text-align: left;background: -moz-linear-gradient( 90deg, rgb(254,0,0) 0%, rgb(254,30,0) 100%);
  background: -webkit-linear-gradient( 90deg, rgb(254,0,0) 0%, rgb(254,38,0) 100%);
  background: -ms-linear-gradient( 90deg, rgb(254,59,0) 0%, rgb(214,87,3) 100%);color:#fff;">
            <th style="padding: 1rem 1rem;" scope="col"><b>Наименование</b></th>
            <th style="padding: 1rem 1rem;text-align:center;" scope="col"><b>Цена за ед.</b></th>
            <th style="padding: 1rem 1rem;text-align:center;" scope="col"><b>Кол-во</b></th>
            <th style="padding: 1rem 1rem;text-align:center;" scope="col"><b>Сумма</b></th>
        </tr>
        </thead>
        <div>
            <tbody style="overflow-x: auto">
            <? $m = 0; ?>
            <? foreach ($orderedProducts as $item): ?>
                <? $m++; ?>
                <tr style="<?= $m % 2 == 1 ? "text-align: left;background-color:#fff;" : "text-align: left"; ?>">
                    <th style="padding: 1rem 1rem;" scope="row"><?= $item->product->name; ?> </th>
                    <td style="padding: 1rem 1rem;text-align:center;"><?= $item->detail->price; ?> тг</td>
                    <td style="padding: 1rem 1rem;text-align:center;"><?= $item->count; ?></td>
                    <td style="padding: 1rem 1rem;text-align:center;"><?= $item->getSumCount($item->detail->price, $item->count); ?>
                        тг
                    </td>
                </tr>
            <? endforeach; ?>

            <tr class="table-price">
                <td></td>
                <td></td>
                <td style="padding: 1rem 1rem;text-align-right;">Итого товара на сумму: <b> <?= $order->sum ?> тг</b>
                </td>
            </tr>
            </tbody>
        </div>
        <style>
        </style>
    </table>
    <br>
    <div style="width: 40%;margin:0 auto;text-align:center;">
        <p>С уважением <br> администрация <a href="<?= $host; ?>" style="color:#fe1500;"><b>POMIDORKA</b></a></p>
    </div>
</div>

<style>
    @media only screen and (max-width: 576px) {
        .table-price {
            display: block !important;
        }

        .table-price td {
            text-align: left !important;
            display: block !important;
        }

        .table-price tr {
            font-size: 11px !important;
        }
    }
</style>
