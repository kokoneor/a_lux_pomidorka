<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\Address;
use common\models\Cities;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Адреса';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'city_id',
                'filter'    => Cities::getList(),
                'value' => function ($model) {
                    return
                        Html::a($model->city->name, ['/cities/view', 'id' => $model->city->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => Address::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Address::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'address',
//            'info:ntext',
            //'longitude:ntext',
            //'latitude:ntext',
            //'sort',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
