<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderedProducts */

$this->title = 'Создание Ordered Products';
$this->params['breadcrumbs'][] = ['label' => 'Ordered Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordered-products-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
