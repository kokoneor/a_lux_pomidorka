<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Catalog;

/* @var $this yii\web\View */
/* @var $model common\models\Catalog */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
//            'parent_id',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Catalog::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'name',
            'slug',
            [
                'attribute' => 'main',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Catalog::mainStatusDescription(), $model->main);
                },
                'format' => 'raw',
            ],
//            'content:ntext',
//            'image',
//            'level',
//            'sort',
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
            'create_at',
        ],
    ]) ?>

</div>
