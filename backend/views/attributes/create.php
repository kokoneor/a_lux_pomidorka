<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Attributes */

$this->title = 'Создание Аттрибуты продукта';
$this->params['breadcrumbs'][] = ['label' => 'Аттрибуты продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attributes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
