<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Menu;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'key')->textInput([
            'maxlength' => true,
            'disabled'  => $model->isNewRecord ? false : true,
        ]) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'url')->textInput([
            'maxlength' => true,
            'disabled'  => $model->isNewRecord ? false : true,
        ]) ?>

        <?= $form->field($model, 'status')->dropDownList(Menu::statusDescription()) ?>

        <?= $form->field($model, 'position')->dropDownList(Menu::positionDescription()) ?>

        <?= $form->field($model, 'type')->dropDownList(Menu::typeDescription()) ?>

        <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
