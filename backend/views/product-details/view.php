<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Детали продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-details-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/product/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'type_id',
                'value' => function ($model) {
                    return
                        Html::a($model->type->name, ['/types/view', 'id' => $model->type->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'attribute_id',
                'value' => function ($model) {
                    return
                        Html::a($model->attribute0->name, ['/attributes/view', 'id' => $model->attribute0->id]);
                },
                'format' => 'raw',
            ],
            'price',
            'created_at',
        ],
    ]) ?>

</div>
