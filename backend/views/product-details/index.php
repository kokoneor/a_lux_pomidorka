<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Детали продукта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-details-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/product/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'type_id',
                'value' => function ($model) {
                    return  $model->type_id !== null
                        ? Html::a($model->type->name, ['/types/view', 'id' => $model->type->id])
                        : ' ';
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'attribute_id',
                'value' => function ($model) {
                    return  $model->attribute_id !== null
                        ? Html::a($model->attribute0->name, ['/attributes/view', 'id' => $model->attribute0->id])
                        : ' ';
                },
                'format' => 'raw',
            ],
            'price',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
