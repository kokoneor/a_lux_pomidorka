<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Emails;

/* @var $this yii\web\View */
/* @var $model common\models\Emails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Элекронная почта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="emails-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'filter' => Emails::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Emails::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'email:email',
            'created_at',
        ],
    ]) ?>

</div>
