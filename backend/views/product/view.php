<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name, ['/catalog/view', 'id' => $model->category->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Product::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'name',
            'code',
            'slug:ntext',
            'content:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>150]);
                }
            ],
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
            'create_at',
        ],
    ]) ?>

</div>
