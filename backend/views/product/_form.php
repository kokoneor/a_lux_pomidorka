<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Catalog;
use kartik\file\FileInput;
use common\models\Types;
use common\models\Attributes;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $details */

$types  = Types::getAll();

$attributes = Attributes::getAll();
?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
        'id' => "product-form"
    ]); ?>

    <?= $form->field($model, 'category_id')->dropDownList(Catalog::getList(), [
        'prompt' => 'Выберите категорию'
    ]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->statusDescription()) ?>

    <input class="product-details-input" type="hidden" name="details" value=''>

    <div id="app">
        <admin-component :details='<?= $model->isNewRecord ? "[{id: null}]" : "$details" ?>' :types='<?= "$types" ?>' :attributes='<?= "$attributes" ?>'></admin-component>
    </div>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload'            => false,
            'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
            'initialPreviewAsData'  => true,
            'initialCaption'        => $model->isNewRecord ? '' : $model->image,
            'showRemove'            => true,
            'deleteUrl'             => \yii\helpers\Url::to(['/product/delete-image', 'id' => $model->id]),
        ],
        'options' => ['accept' => 'image/*'],
    ]); ?>

    <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    Vue.component('admin-component', {
        template: `
        <div>
            <h2>Детали продукта <button type="button" @click="addDetails();">+</button></h2>
            <div class="product-details" v-for="(detail, index) in reactiveDetails" :data-id="detail.id" :key="detail.id" style="display: flex; align-items: center; justify-content: space-between">
                <div style="flex: 0 0 94%; display: flex; align-items: center; justify-content: space-between;">
                    <div class="form-group field-product-status" style="flex: 0 0 33%">
                        <label class="control-label" for="product-types">Детали</label>
                        <select class="form-control product-types" v-model="detail.type">
                        <option value="">Не выбрано типов</option>
                            <option v-for="(type, index) in types" :key="index" :value="type.id">{{ type.name }}</option>
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-product-status" style="flex: 0 0 33%">
                        <label class="control-label" for="product-attributes">Атрибуты</label>
                        <select class="product-attributes form-control" v-model="detail.attribute">
                            <option value="">Не выбрано атрибутов</option>
                            <option v-for="(attribute, index) in attributes" :key="index" :value="attribute.id">{{ attribute.name }}</option>
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-product-status" style="flex: 0 0 33%">
                        <label class="control-label" for="product-price">Цена</label>
                        <input required type="number" class="product-price form-control" v-model="detail.price">
                        <div class="help-block"></div>
                    </div>
                </div>
                <div style="flex: 0 0 5%; display: flex; align-items: center; justify-content:space-between;">
                    <button type="button" class="decrease" @click="removeDetail(detail.id)">-</button>
                </div>
            </div>
        </div>
        `,
        data() {
            return {
                reactiveDetails: []
            }
        },
        props: {
            types: {
                type: Array
            },
            attributes: {
                type: Array
            },
            details: {
                type: Array,
                default: [{ id: null}]
            }
        },
        methods: {
            addDetails() {
                return this.reactiveDetails.push({
                    id: null,
                })
            },
            removeDetail(id) {
                let index = this.reactiveDetails.findIndex(detail => detail.id == id);
                if (index != -1) {
                    this.reactiveDetails.splice(index, 1);
                }
            },
        },
        created() {
            this.reactiveDetails = this.details;
        }
    });
    const app = new Vue({
        el: "#app",
    });
</script>
<script>
    document.querySelector('[type="submit"]').addEventListener('click', function(e) {
        let detailsArr = [],
            productTypes = document.querySelectorAll('.product-types'),
            productPrices = document.querySelectorAll('.product-price'),
            productAttributes = document.querySelectorAll('.product-attributes'),
            productDetails = document.querySelectorAll('.product-details'),
            productDetailsInput = document.querySelector('.product-details-input');
        productDetails.forEach((detail, index) => {
            detailsArr.push({
                id: detail.getAttribute('data-id'),
                type: productTypes[index].value,
                attribute: productAttributes[index].value,
                price: productPrices[index].value
            })
        });
        productDetailsInput.value = JSON.stringify(detailsArr);
        // document.getElementById('product-form').submit();
    });
</script>