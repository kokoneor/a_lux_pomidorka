<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Orders;
use common\models\OrderedProducts;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $orderedProducts OrderedProducts */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return
                        Html::a($model->user->email, ['/users/view', 'id' => $model->user->id]);
                },
                'format' => 'raw',
            ],
            'sum',
            'status',
            [
                'attribute' => 'status',
                'filter' => Orders::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'username',
            'phone',
            'email:email',
            'address',
            'apartment',
            'storey',
            'porch',
            'intercom',
            'message:ntext',
            'amountMoney',
            [
                'attribute' => 'statusPay',
                'filter' => Orders::payDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::payDescription(), $model->statusPay);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'typePay',
                'filter' => Orders::typeDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::typeDescription(), $model->typePay);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'typeDelivery',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::deliveryDescription(), $model->typeDelivery);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'address_id',
                'value' => function ($model) {
                    return  $model->delivery != null
                        ? Html::a($model->delivery->address, ['/address/view', 'id' => $model->delivery->id])
                        : ' ';
                },
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Продукты заказа:</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
<!--                    <th>Детали </th>-->
                    <th>Количества</th>
                    <th>Цена</th>
                </tr>
                </thead>
                <tbody>
                <? if($orderedProducts != null):?>
                    <? foreach ($orderedProducts as $v):?>

                        <? if($v->product != null):?>
                            <tr>
                                <td>
                                    <a href="/admin/product/view?id=<?=$v->product->id;?>">
                                        <img src="<?=$v->product->getImage();?>" width="50">
                                        <?=$v->product->name;?>
                                    </a>
                                </td>

<!--                                <td>-->
<!--                                    --><?//= $v->detail->type_id !== null
//                                            ? $v->detail->type->name
//                                            : ''
//                                        . ' ' .
//                                        $v->detail->attribute_id !== null
//                                            ? $v->detail->attribute0->name
//                                            : ''
//                                        ;?>
<!--                                </td>-->
                                <td><?=$v->count;?></td>
                                <td><?=$v->product->price*$v->count;?></td>
                            </tr>
                        <? else:?>
                            <tr>
                                <td>Не найдено, ID:<?=$v->product_id;?></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <? endif;?>
                    <? endforeach;?>
                <? endif;?>
                </tbody>
            </table>
        </div>
    </div>

</div>
