<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Orders;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'order_id',
//            'user_id',
            'sum',
//            'status',
            'username',
            'phone',
            'email:email',
            'address',
            //'apartment',
            //'storey',
            //'porch',
            //'intercom',
            //'message:ntext',
            //'amountMoney',
            //'statusPay',
            [
                'attribute' => 'statusPay',
                'filter' => Orders::payDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::payDescription(), $model->statusPay);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'typeDelivery',
                'filter' => Orders::deliveryDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Orders::deliveryDescription(), $model->typeDelivery);
                },
                'format' => 'raw',
            ],
            //'typePay',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
