<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form of `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'user_id', 'sum', 'status', 'statusPay', 'typePay', 'typeDelivery', 'address_id'], 'integer'],
            [['username', 'phone', 'email', 'address', 'apartment', 'storey', 'porch', 'intercom', 'message', 'amountMoney', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'sum' => $this->sum,
            'status' => $this->status,
            'statusPay' => $this->statusPay,
            'typePay' => $this->typePay,
            'typeDelivery' => $this->typeDelivery,
            'address_id' => $this->address_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'apartment', $this->apartment])
            ->andFilterWhere(['like', 'storey', $this->storey])
            ->andFilterWhere(['like', 'porch', $this->porch])
            ->andFilterWhere(['like', 'intercom', $this->intercom])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'amountMoney', $this->amountMoney]);

        return $dataProvider;
    }
}
