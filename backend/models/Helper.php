<?php

namespace backend\models;

use yii\web\UploadedFile;

class Helper
{
    protected function createImage($model)
    {
        $image = UploadedFile::getInstance($model, 'image');
        if ($image !== null) {
            $time = time();
            $image->saveAs($model->path() . $time . '_' . $image->baseName . '.' . $image->extension);
            $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
        }
    }

    protected function updateImage($model, $oldImage)
    {
        $image = UploadedFile::getInstance($model, 'image');

        if ($image == null) {
            $model->image = $oldImage;
        } else {
            $time = time();
            $image->saveAs($model->path() . $time . '_' . $image->baseName . '.' . $image->extension);
            $model->image = $time . '_' . $image->baseName . '.' . $image->extension;

            if (!empty($oldImage) && file_exists($model->path() . $oldImage)) {
                unlink($model->path() . $oldImage);
            }
        }
    }

    protected function deleteImage($model)
    {
        $oldImage = $model->image;

        if (file_exists($model->path . $oldImage)) {
            unlink($model->path . $oldImage);
        }
    }

    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }
}