<?php

namespace backend\controllers;

use Yii;
use common\models\Delivery;
use backend\models\search\DeliverySearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeliveryController implements the CRUD actions for Delivery model.
 */
class DeliveryController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createTranslation',
        'view'   => 'viewTranslation',
        'update' => 'updateTranslation',
        'index'  => 'indexTranslation',
        'delete' => 'deleteTranslation',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Delivery::className();
            $this->searchModel = DeliverySearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new About model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }
        $model = new Delivery();

        if ($model->load(Yii::$app->request->post())) {
            $this->createImage($model);

            if($model->save()) {
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing About model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model = $this->findModel($id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            $this->updateImage($model, $oldImage);

            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
