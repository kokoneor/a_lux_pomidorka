<?php

namespace backend\controllers;

use Yii;
use common\models\Emails;
use backend\models\search\EmailsSearch;
use yii\base\Action;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmailsController implements the CRUD actions for Emails model.
 */
class EmailsController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Emails::className();
            $this->searchModel = EmailsSearch::className();

            return true;
        }

        return false;
    }
}
