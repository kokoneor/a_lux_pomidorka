<?php

namespace backend\controllers;

use Yii;
use common\models\PriceMinimum;
use backend\models\search\PriceMinimumSearch;
use yii\base\Action;
use yii\web\Controller;

/**
 * PriceMinimumController implements the CRUD actions for PriceMinimum model.
 */
class PriceMinimumController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = PriceMinimum::className();
            $this->searchModel = PriceMinimumSearch::className();

            return true;
        }

        return false;
    }
}
