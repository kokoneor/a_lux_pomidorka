<?php

namespace backend\controllers;

use Yii;
use common\models\QuestionRequest;
use backend\models\search\QuestionRequestSearch;
use yii\base\Action;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuestionRequestController implements the CRUD actions for QuestionRequest model.
 */
class QuestionRequestController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = QuestionRequest::className();
            $this->searchModel = QuestionRequestSearch::className();

            return true;
        }

        return false;
    }
}
