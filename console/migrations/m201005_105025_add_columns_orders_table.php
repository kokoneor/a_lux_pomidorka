<?php

use yii\db\Migration;

/**
 * Class m201005_105025_add_columns_orders_table
 */
class m201005_105025_add_columns_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'typeDelivery', $this->integer()->null());
        $this->addColumn('orders', 'address_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'typeDelivery');
        $this->dropColumn('orders', 'address_id');
    }
}
