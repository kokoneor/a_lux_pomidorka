<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%emails}}`.
 */
class m200908_061717_create_emails_table extends Migration
{
    public $table               = 'emails';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'status'            => $this->integer()->defaultValue(0)->null(),
            'email'             => $this->string(255)->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
