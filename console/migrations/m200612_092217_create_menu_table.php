<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%menu}}`.
 */
class m200612_092217_create_menu_table extends Migration
{
    public $table               = 'menu';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                    => $this->primaryKey(),
            'key'                   => $this->string(255)->null(),
            'name'                  => $this->string(255)->notNull(),
            'url'                   => $this->string(255)->notNull(),
            'status'                => $this->integer()->notNull()->defaultValue(1),
            'position'              => $this->integer()->null()->defaultValue(0),
            'type'                  => $this->integer()->null()->defaultValue(1),
            'content'               => $this->text()->null(),
            'sort'                  => $this->integer()->null(),
            'metaName'              => $this->string(255)->null(),
            'metaDesc'              => $this->text()->null(),
            'metaKey'               => $this->text()->null(),
            'created_at'            => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
