<?php

use yii\db\Migration;

/**
 * Class m200615_060223_add_columns_catalog_table
 */
class m200615_060223_add_columns_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('catalog', 'main', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('catalog', 'main');
    }
}
