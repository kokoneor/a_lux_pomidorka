<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stock}}`.
 */
class m200407_065843_create_stock_table extends Migration
{
    public $table               = 'stock';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'status'        => $this->integer()->defaultValue(1)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'slug'          => $this->text()->notNull(),
            'title'         => $this->string(255)->null(),
            'content'       => $this->text()->null(),
            'image'         => $this->string(255)->null(),
            'sort'          => $this->integer()->null(),
            'metaName'      => $this->string(255)->null(),
            'metaDesc'      => $this->text()->null(),
            'metaKey'       => $this->text()->null(),
            'create_at'     => $this->timestamp()->null(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
