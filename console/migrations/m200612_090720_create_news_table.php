<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m200612_090720_create_news_table extends Migration
{
    public $table               = 'news';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'status'        => $this->integer()->defaultValue(0)->notNull(),
            'title'         => $this->string(255)->notNull(),
            'slug'          => $this->text()->null(),
            'description'   => $this->text()->null(),
            'content'       => $this->text()->null(),
            'image'         => $this->text()->null(),
            'metaName'      => $this->string(255)->null(),
            'metaDesc'      => $this->text()->null(),
            'metaKey'       => $this->text()->null(),
            'date'          => $this->date()->null(),
            'created_at'    => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
