<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%feedback}}`.
 */
class m200614_070850_create_feedback_table extends Migration
{
    public $table               = 'feedback';
    public $userTable           = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'user_id'           => $this->integer()->null(),
            'email'             => $this->string(255)->null(),
            'phone'             => $this->string(255)->null(),
            'theme'             => $this->text()->null(),
            'message'           => $this->text()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->userTable}",
            "{{{$this->table}}}", 'user_id',
            "{{{$this->userTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->userTable}", "{{{$this->userTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
