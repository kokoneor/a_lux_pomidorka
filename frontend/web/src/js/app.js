import Vue from "vue";
import Vuellidate from "vuelidate";
import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";
import VModal from "vue-js-modal";
import VueRouter from "vue-router";
import store from "./vuex/store.js";
import VueTheMask from "vue-the-mask";
import VueLocalStorage from "vue-localstorage";
import { CollapsePlugin } from "bootstrap-vue";
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)
Vue.use(VueLocalStorage);
Vue.use(CollapsePlugin);
Vue.use(VModal, { dynamic: true, injectModalsContainer: true });
Vue.use(Vuellidate);
Vue.use(VueRouter);
Vue.use(VueTheMask);

import CartLinkComponent from "./components/CartLinkComponent.vue";
import AuthLinkComponent from "./components/AuthLinkComponent.vue";
import MainComponent from "./components/MainComponent.vue";
import StocksComponent from "./components/StocksComponent.vue";
import ProductCardComponent from "./components/ProductCardComponent.vue";
import RestoreComponent from "./components/RestoreComponent.vue";
import CatalogComponent from "./components/CatalogComponent.vue";
import AppLinkComponent from "./components/AppLinkComponent.vue";

//components
Vue.component("cart-link-component", CartLinkComponent);
Vue.component("auth-link-component", AuthLinkComponent);
Vue.component("main-component", MainComponent);
Vue.component("stocks-component", StocksComponent);
Vue.component("product-card-component", ProductCardComponent);
Vue.component("restore-component", RestoreComponent);
Vue.component("catalog-component", CatalogComponent);
Vue.component("app-link-component", AppLinkComponent);

const requireComponent = require.context(
  "./components/base",
  false,
  /Base[A-Z]\w+\.(vue|js)$/
);
requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName);
  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, "$1"))
  );
  Vue.component(componentName, componentConfig.default || componentConfig);
});

// views
import Profile from "./views/Profile.vue";
import History from "./views/History.vue";
import Delivery from "./views/Delivery.vue";
import Payment from "./views/Payment.vue";
import Edit from "./views/Edit.vue";
import Authentication from "./views/Authentication.vue";
import Login from "./views/Login.vue";
import Registration from "./views/Registration.vue";
import Order from "./views/Order.vue";
import OrderCart from "./views/OrderCart.vue";
import OrderPayment from "./views/OrderPayment.vue";
import OrderDelivery from "./views/OrderDelivery.vue";
// routes
const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/authentication/login",
      name: "login",
      component: Login,
    },
    {
      path: "/authentication/registration",
      name: "registration",
      component: Registration,
    },
    {
      path: "/profile/history",
      name: "history",
      component: History,
    },
    {
      path: "/profile/delivery",
      name: "delivery",
      component: Delivery,
    },
    {
      path: "/profile/payment",
      name: "payment",
      component: Payment,
    },
    {
      path: "/profile/edit",
      name: "edit",
      component: Edit,
    },
    {
      path: "/order",
      name: "orderCart",
      component: OrderCart,
    },
    {
      path: "/order/payment",
      name: "orderPayment",
      component: OrderPayment,
    },
    {
      path: "/order/delivery",
      name: "orderDelivery",
      component: OrderDelivery,
    },
  ],
});

const app = new Vue({
  el: "#app",
  store,
  components: { Profile, Authentication, Order },
  router,
  localStorage: {
    attributesInCart: {
      type: Array,
      default: [],
    },
  },
});
