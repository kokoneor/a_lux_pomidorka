window.$ = window.jQuery = require("jquery");
require("hc-offcanvas-nav");
import slick from "slick-carousel";
import { collapse } from "bootstrap";
import axios from 'axios'
import Swal from 'sweetalert2'
import mask from 'jquery-mask-plugin'

const $main_nav = $(".categories");
const $toggle = $(".toggle");
const ask_btn = document.querySelector('.ask-btn')
const categories = document.querySelectorAll(
  ".header .categories:not(.hc-offcanvas-nav) ul li a"
);

let call_form = document.querySelector('.call_form')
let call_username = document.querySelector('#call_username')
let call_telephone = document.querySelector('#call_telephone')
let call_message = document.querySelector('#call_message')

if (call_telephone) {
  $('#call_telephone').mask('+7 ### ### ####');
}

categories.forEach((category) => {
  if (category.href == window.location.href) {
    category.classList.add("active");
  }
});

if (ask_btn) {
  ask_btn.addEventListener('click', () => {
    $('#modalcall').modal('show')
  })
}
if (call_form) {
  call_form.addEventListener('submit', e => {
    e.preventDefault()
    axios
      .post('/api/question-request/create', {
        username: call_username.value,
        phone: call_telephone.value,
        message: call_message.value
      })
      .then(res => {
        Swal.fire(res.data.data.message, '', 'success')
        $('#modalcall').modal('hide')
      })
      .catch(e => Swal.fire(e.message, 'error'))
  })
}


let defaultOptions = {
  disableAt: 767,
  customToggle: $toggle,
  navTitle: "Каталог",
  levelTitles: true,
  levelTitleAsBack: true,
  pushContent: "body > *",
  insertClose: false,
  closeLevels: false,
};

$main_nav.hcOffcanvasNav(defaultOptions);

const $banner = $("#main-banner .slider");
$banner.slick({
  slidesToShow: 1,
  dots: true,
  swipeToSlide: true,
  autoplay: false,
  pauseOnHover: true,
  // autoplaySpeed: 2500,
  arrows: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        dots: true,
        arrows: false,
      },
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
      },
    },
    {
      breakpoint: 578,
      settings: {
        dots: false,
        arrows: false,
      },
    },
  ],
});

let durationList = $(".slider .item").map(function(index, item) {
  return item.getAttribute("data-delay") + "000";
});

var slideIndex = 0;
var changeSlide = function(timing) {
  setTimeout(function() {
    if (timing !== 0) $banner.slick("slickNext");
    if (slideIndex >= durationList.length) slideIndex = 0; //Start from beginning?
    changeSlide(durationList[slideIndex++]); //Calls itself with duration for next slide
  }, timing);
};
changeSlide(0);
const contactMap = document.getElementById("map");
let addresses = document.querySelectorAll(".info.address a");
if (contactMap) {
  function init() {
    let map = new ymaps.Map("map", {
      center: [43.25622887, 76.92696959],
      zoom: 13,
    });
    addresses.forEach((address) => {
      map.geoObjects.add(
        new ymaps.Placemark(
          [
            address.getAttribute("data-latitude"),
            address.getAttribute("data-longitude"),
          ],
          {
            balloonContent: address.getAttribute("data-info"),
          },
          {
            iconLayout: "default#image",
            iconImageHref: "/images/placeholder.png",
            iconImageSize: [50, 50],
            iconImageOffset: [-25, -50],
            preset: "islands#icon",
            iconColor: "#DE293A",
          }
        )
      );
    });
  }
  ymaps.ready(init);
}
