import Vue from "vue";
import Vuex from "vuex";
import { ProductService, AuthService } from "../services/service.js";
import Swal from "sweetalert2";
const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 1500,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: {},
    stocks: {},
    userData: [],
    orders: [],
    isAuth: false,
    productsInCart: [],
    productsInCartCount: null,
    totalPrice: null,
    typePay: 4,
    typeDelivery: 0,
  },
  mutations: {
    SET_PRODUCTS(state, products) {
      state.products = products;
    },
    SET_STOCKS(state, stocks) {
      state.stocks = stocks;
    },
    SET_USERDATA(state, { userData, isAuth }) {
      state.userData = userData;
      state.isAuth = isAuth;
    },
    SET_PRODUCTS_IN_BASKET(state, { products, count, total_price }) {
      state.productsInCart = products;
      state.productsInCartCount = count;
      state.totalPrice = total_price;
    },
    INCREASE_TOTAL(state, { price, count }) {
      state.productsInCartCount += count;
      state.totalPrice += price;
    },
    DECREASE_TOTAL(state, { price, count }) {
      state.productsInCartCount -= count;
      state.totalPrice -= price;
    },
    SET_TYPEPAY(state, pay_type) {
      state.typePay = pay_type;
    },
    SET_TYPEDELIVERY(state, type_delivery) {
      state.typeDelivery = type_delivery
    },
    SET_ORDERS(state, orders) {
      state.orders = orders;
    },
    SET_USER_ADRESS(state, address) {
      state.userData.address = address.address;
      state.userData.address_id = address.address_id; 
      state.userData.apartment = address.apartment;
      state.userData.intercom = address.intercom;
      state.userData.storey = address.storey;
      state.userData.porch = address.porch;
    },
  },
  actions: {
    fetchProducts({ commit }, category_id) {
      ProductService.getProducts(category_id).then((response) => {
        commit("SET_PRODUCTS", response.data.data.catalog);
      });
    },
    fetchStocks({ commit }) {
      ProductService.getStocks().then((response) => {
        commit("SET_STOCKS", response.data.data);
      });
    },
    fetchCartProducts({ commit, state }) {
      ProductService.getCartProducts(state.productsInCart)
        .then((response) => {
          commit("SET_PRODUCTS_IN_BASKET", {
            products: response.data.data.products,
            count: response.data.data.total_count,
            total_price: response.data.data.total_price,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    },
    createAddress({ commit }, address) {
      AuthService.createAddress(address).then((response) => {
        Toast.fire({
          icon: "success",
          title: response.data.data.message,
        });
        commit("SET_USER_ADRESS", response.data.data.address[0]);
      });
    },
    fetchAddress({ state }) {
      AuthService.getAddress().then((response) => {
        console.log(response.data.data);
      });
    },
    authCheck({ commit, state }) {
      AuthService.authCheck()
        .then((response) => {
          if (response.data.success) {
            commit("SET_USERDATA", {
              userData: response.data.data,
              isAuth: true,
            });
            state.userData.birthday = new Date(response.data.data.birthday);
          } else if (
            !response.data.success &&
            response.data.data.status == 401 &&
            (window.location.href.indexOf("profile") != -1 ||
              window.location.href.indexOf("order") != -1)
          ) {
            window.location.replace("/authentication/login");
          }
        })
        .catch((error) => {
          if (
            error.status == 401 &&
            (window.location.href.indexOf("profile") != -1 ||
              window.location.href.indexOf("order") != -1)
          ) {
            window.location.replace("/authentication/login");
          }
        });
    },
    profileUpdate({ commit, state }, userData) {
      AuthService.update(userData).then((response) => {
        if (response.data.success) {
          commit("SET_USERDATA", {
            userData: response.data.data.user,
            isAuth: true,
          });
          state.userData.birthday = new Date(response.data.data.user.birthday);
        }
      });
    },
    logout({ commit }) {
      localStorage.removeItem("token");
      localStorage.removeItem("attributesInCart");
      window.location.replace(window.location.origin);
    },
    removeProduct({ state }, detail_id) {
      let attributesInCart = JSON.parse(
        localStorage.getItem("attributesInCart")
      );
      let index = state.productsInCart.findIndex(
        (product) => product.detail_id == detail_id
      );
      if (index > -1) {
        state.productsInCart.splice(index, 1);
        attributesInCart.splice(attributesInCart.indexOf(detail_id), 1);
      }
      localStorage.setItem(
        "attributesInCart",
        JSON.stringify(attributesInCart)
      );
      let sum = 0;
      let quantity = 0;
      for (const index in state.productsInCart) {
        sum +=
          state.productsInCart[index].price *
          state.productsInCart[index].quantity;
        quantity += state.productsInCart[index].quantity;
      }
      state.productsInCartCount = quantity;
      state.totalPrice = sum;
    },
    clearCart({ state }) {
      state.productsInCart = [];
      state.productsInCartCount = 0;
      state.totalPrice = 0;
      localStorage.setItem("attributesInCart", []);
    },
    checkout({ state }) {
      ProductService.checkout(
        state.userData.username,
        state.userData.phone,
        state.userData.email,
        state.userData.message,
        state.totalPrice,
        state.userData.address,
        state.userData.address_id,
        state.userData.apartment,
        state.userData.floor,
        state.userData.porch,
        state.userData.intercom,
        state.typePay,
        state.productsInCart,
        state.typeDelivery,
      )
        .then((response) => {
          if (response.data.success) {
            localStorage.setItem("attributesInCart", []);
            Toast.fire({
              icon: "success",
              title: response.data.data.message,
            }).then(() => {
              if (!response.data.data.url) {
                window.location.replace("/profile/history");
              } else if (response.data.data.url) {
                window.location.replace(response.data.data.url);
              }
            });
          } else {
            Toast.fire({
              icon: "warning",
              title: response.data.data.message,
            });
          }
        })
        .catch((error) => {
          if (error.status == 403) {
            Toast.fire({
              icon: "warning",
              title: error.data.data.message,
            });
          }
        });
    },
    fetchOrders({ commit }) {
      AuthService.getOrders().then((response) => {
        commit("SET_ORDERS", response.data.data);
      });
    },
  },
  getters: {},
});
