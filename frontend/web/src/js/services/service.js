import axios from "axios";
import NProgress from "nprogress";

const apiClient = axios.create({
  baseURL: window.location.origin,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

const token = localStorage.getItem("token");
const config = {
  headers: { Authorization: `Bearer ${token}` },
};

apiClient.interceptors.request.use((config) => {
  NProgress.start();
  return config;
});

apiClient.interceptors.response.use(
  (response) => {
    NProgress.done();
    return response;
  },
  function(error) {
    NProgress.done();
    return Promise.reject(error.response);
  }
);

export class AuthService {
  static register(userData) {
    return apiClient.post(`/api/sign-up`, userData);
  }
  static login(userData) {
    return apiClient.post(`/api/site/login`, userData);
  }
  static authCheck() {
    return apiClient.get(`/api/profile`, config);
  }
  static update(userData) {
    return apiClient.post(`/api/profile/update`, userData, config);
  }
  static getOrders() {
    return apiClient.get("/api/order", config);
  }
  static createAddress(address) {
    return apiClient.post("api/user-address/create", address, config);
  }
  static restorePassword(login) {
    return apiClient.post("/api/password-reset", login);
  }
}

export class ProductService {
  static getProducts(category_id) {
    if (!category_id) {
      return apiClient.get("/api/catalog/main");
    } else {
      return apiClient.get(`/api/catalog/view?id=${category_id}`);
    }
  }
  static getStocks() {
    return apiClient.get("/api/stock");
  }
  static getProduct(id) {
    return apiClient.get(`/api/product/view?id=${id}`);
  }
  static getCartProducts(ids) {
    return apiClient.get("/api/basket", {
      params: {
        array: ids,
      },
    });
  }
  static checkout(
    username,
    phone,
    email,
    message,
    sum,
    address,
    address_id,
    apartment,
    storey,
    porch,
    intercom,
    typePay,
    orderProducts,
    typeDelivery,
  ) {
    return apiClient.post(
      "/api/order/create",
      {
        username: username,
        phone: phone,
        email: email,
        message: message,
        sum: sum,
        address: (typeDelivery == 1) ? address : 'Самовывоз',
        apartment: apartment,
        storey: storey,
        porch: porch,
        intercom: intercom,
        typePay: typePay,
        orderProducts: orderProducts,
        typeDelivery: typeDelivery,
        address_id: address_id
      },
      config
    );
  }
}
