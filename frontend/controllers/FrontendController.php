<?php

namespace frontend\controllers;

use common\models\Catalog;
use common\models\City;
use common\models\Contact;
use common\models\Contacts;
use common\models\Documents;
use common\models\Language;
use common\models\Logo;
use common\models\Menu;
use common\models\SocialNetworks;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

class FrontendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function init()
    {
        $menu  = Menu::findKey('main');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $menuHeader                 = Menu::getHeader();
        $menuInformationFooter      = Menu::getInformationFooter();
        $menuClientFooter           = Menu::getClientFooter();
        $categories                 = Catalog::getAll();
        $logoHeader                 = Logo::getHeader();
        $logoFooter                 = Logo::getFooter();
        $contact                    = Contacts::getOne();
        $socialNetworks             = SocialNetworks::getAll();


        \Yii::$app->view->params['menuHeader']              = $menuHeader;
        \Yii::$app->view->params['menuInformationFooter']   = $menuInformationFooter;
        \Yii::$app->view->params['menuClientFooter']        = $menuClientFooter;
        \Yii::$app->view->params['categories']              = $categories;
        \Yii::$app->view->params['logoHeader']              = $logoHeader;
        \Yii::$app->view->params['logoFooter']              = $logoFooter;
        \Yii::$app->view->params['contact']                 = $contact;
        \Yii::$app->view->params['socialNetworks']          = $socialNetworks;
    }

    protected function setMeta($title = null, $description = null, $keywords = null)
    {
        $this->view->title      =   $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
    }
}