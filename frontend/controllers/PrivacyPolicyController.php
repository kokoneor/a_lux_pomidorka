<?php

namespace frontend\controllers;

use common\models\Menu;
use common\models\PrivacyPolicy;

class PrivacyPolicyController extends FrontendController
{
    public function actionIndex()
    {
        $menu  = Menu::findKey('privacy-policy');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $model  = PrivacyPolicy::getOne();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}