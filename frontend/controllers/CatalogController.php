<?php

namespace frontend\controllers;

use common\models\Catalog;
use yii\web\NotFoundHttpException;

/**
 * Catalog controller
 */

class CatalogController extends FrontendController
{

    /**
     * Catalog display index.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index', []);
    }

    /**
     * Displays a single model.
     * @param $slug
     * @return string
     * @throws
     */
    public function actionView($slug)
    {
        $model      = Catalog::findOne(['slug' => $slug]);
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);

        $category_id    = $model->id;
        $slug           = $model->slug;


        return $this->render('view', [
            'model'         => $model,
            'category_id'   => $category_id,
            'slug'          => $slug,
        ]);
    }
}
