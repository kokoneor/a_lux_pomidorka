<?php
namespace frontend\models;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use common\models\User;

/**
 * PasswordUpdate
 */
class SecureForm extends Model
{
    public  $password;
    public  $new_password;
    public  $new_password_repeat;
    private $_user;

//    public function __construct()
//    {
//        $this->_user      = User::findOne(['id' => \Yii::$app->user->identity->id]);
//        $this->attributes = $this->_user->attributes;
//
//        parent::__construct();
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password'            => \Yii::t('main-label', 'Старый пароль'),
            'new_password'        => \Yii::t('main-label', 'Новый пароль'),
            'new_password_repeat' => \Yii::t('main-label', 'Подтверждение нового пароля'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'new_password', 'new_password_repeat'], 'required'],
            ['password', 'validatePassword'],
            ['new_password', 'string', 'min' => 8],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => \Yii::t('main-error', 'Пароли не совпадают')],
        ];
    }

    public function updatePassword()
    {
        if (!$this->validate()) {
            return null;
        }

        if($user   = User::findOne(['id' => \Yii::$app->user->identity->id])){
            $user->setPassword($this->new_password);
            $user->save();
            return $user;
        }

        return null;

    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Старый пароль не соответсвует.');
            }
        }
    }

//    /**
//     * @return bool
//     */
//    public function save()
//    {
//        $this->_user->setPassword($this->new_password);
//
//        return $this->_user->save(false);
//    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['id' => \Yii::$app->user->identity->id]);
        }

        return $this->_user;
    }

}