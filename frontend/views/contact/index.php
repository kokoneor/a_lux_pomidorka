<?php

/* @var $this View */
/* @var $address Address */
/* @var $contact Contacts */
/* @var $faq Faq */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Address;
use common\models\Contacts;
use common\models\Faq;



?>

<div class="contacts py-5">
    <div class="container">
        <div class="row mb-4">
            <div class="col-lg-12">
                <h1>Контакты</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ">
                <div id="map"></div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-lg-4">
                <div class="info phone">
                    <a href="tel:<?= $contact->phone; ?>"><?= $contact->phone; ?></a>
                    <p>
                        <?= Yii::t('main-contact', 'Сделайте заказ по телефону'); ?>
                    </p>
                </div>
            </div>
            <? if(!empty($contact->email)): ?>
                <div class="col-lg-4">
                    <div class="info mail">
                        <a href="mailto:<?= $contact->email; ?>"><?= $contact->email; ?></a>
                        <p>
                            <?= Yii::t('main-contact', 'Вопросы, отзывы и предложения по улучшению сервиса и качества'); ?>
                        </p>
                    </div>
                </div>
            <? endif; ?>
            <div class="col-lg-4">
                <div class="info address">
                    <div class="address-mark">

                        <? foreach ($address as $item): ?>
                        <a href="javascript:void(0)" data-longitude="<?= $item->longitude ?>" data-latitude="<?= $item->latitude; ?>" data-info="<?= $item->info; ?>">
                            <?= $item->address; ?>
                        </a>
                        <? endforeach; ?>
                    </div>

                    <p>
                        <?= Yii::t('main-contact', 'Приезжайте к нам'); ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 my-5">
                <h2>
                    <?= Yii::t('main-contact', 'Часто задавемые вопросы:'); ?>
                </h2>
            </div>
            <div class="col-lg-12">
                <div class="accordion" id="accordion">
                    <? foreach ($faq as $item): ?>
                    <div class="card">
                        <div class="card-header" id="heading<?= $item->id ?>" data-toggle="collapse" data-target="#collapse<?= $item->id ?>" aria-expanded="true" aria-controls="collapse<?= $item->id ?>">
                            <span>
                                <?= $item->question ?>
                            </span>
                            <i class="fal fa-plus"></i>
                        </div>
                        <div id="collapse<?= $item->id ?>" class="collapse" aria-labelledby="heading<?= $item->id ?>" data-parent="#accordion">
                            <div class="card-body">
                                <?= $item->answer ?>
                            </div>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
            </div>
            <div class="col-lg-12 mt-5">
                <button class="ask-btn">
                    <?= Yii::t('main-contact', 'Задать свой вопрос'); ?>
                </button>
            </div>
            <div class="modal" id="modalcall" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">форма обратной связи</h5>
                            <div class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </div>
                        </div>
                        <form class="call_form">
                            <div class="modal-body">
                                <div class="modal-body_block">
                                    <h3>Имя</h3>
                                    <input type="text" id="call_username" required />
                                </div>
                                <div class="modal-body_block">
                                    <h3>Номер телефона</h3>
                                    <input type="text" id="call_telephone" required />
                                </div>
                                <div class="modal-body_block">
                                    <h3>Сообщение</h3>
                                    <textarea cols="30" rows="5" id="call_message"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="call_id" class="btn">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>