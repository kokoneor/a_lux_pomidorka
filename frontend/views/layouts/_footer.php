<?php

use yii\helpers\Url;
use common\models\Menu;

$informationMenu    = Yii::$app->view->params['menuInformationFooter'];
$clientMenu         = Yii::$app->view->params['menuClientFooter'];

?>
<div class="footer" class="mt-5">
    <div class="container">
        <div class="row py-4 border-bottom">
            <div class="col-lg-3 col-sm-4 col-12">
                <a href="#" style="display: contents">
                    <img class="logotype" src="<?= \Yii::$app->view->params['logoFooter']->getImage() ?>">
                </a>
                <p class="title">
                    <?= Yii::t('main-footer', 'Доставляем удовольствие каждому клиенту'); ?>
                </p>
            </div>
            <div class="col-lg-2 col-sm-4 col-6">
                <h2>Клиентам</h2>
                <ul>
                    <? foreach ($clientMenu as $menu): ?>
                    <li>
                        <a href="<?= $menu->url; ?>">
                            <?= $menu->name; ?>
                        </a>
                    </li>
                    <? endforeach; ?>
                </ul>
            </div>
            <div class="col-lg-3 col-sm-4 col-6">
                <h2>Информация</h2>
                <ul>
                    <? foreach ($informationMenu as $menu): ?>
                    <li>
                        <a href="<?= $menu->url; ?>">
                            <?= $menu->name; ?>
                        </a>
                    </li>
                    <? endforeach; ?>

                </ul>
            </div>
        </div>
        <div class="row py-4">
            <div class="col-lg-2 col-md-2 col-sm-3 d-flex align-items-center">
                <p class="copyright">
                    <?= Yii::t('main-footer', '© 2020, FARFOR'); ?>
                </p>
            </div>
            <div class="offset-lg-1">
            </div>
            <div class="col-lg-5 col-md-3 col-sm-7 d-flex align-items-center mb-4 mb-md-0">
                <p class="props">
                    <?= Yii::t('main-footer', 'ООО «Территория Еды 102» (ОГРН:1150280063493) Юридический адрес:450071, РБ, г. Уфа, улица Ростовская, дом № 18, литер Б'); ?>
                </p>
            </div>
            <div class="col-lg-1 col-md-2 col-sm-2 col-3">
                <ul class="brands">
                    <? foreach (\Yii::$app->view->params['socialNetworks'] as $network): ?>
                        <li>
                            <a href="<?= $network->link; ?>" target="_blank">
                                <i class="<?= $network->image; ?>"></i>
                            </a>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
            <div class="col-lg-3 col-md-5 col-sm-6 col-9">
                <ul class="apps">
                    <li>
                        <a href="<?= \Yii::$app->view->params['contact']->play_market ?>" target="_blank">
                            <img src="/images/googleplay.png">
                        </a>
                    </li>
                    <li>
                        <a href="<?= \Yii::$app->view->params['contact']->app_store ?>" target="_blank">
                            <img src="/images/appstore.png">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>