<?php

/* @var $this View */
/* @var $banners Banner */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Banner;

?>
<div id="main-banner" class="my-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="slider">
                    <? foreach ($banners as $banner): ?>
                    <a href="<?= $banner->link; ?>">
                        <div>
                            <div class="item" data-delay="<?= $banner->delay ?>">
                                <img src="<?= $banner->getImage() ?>" alt="">
                            </div>
                        </div>
                    </a>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<main-component></main-component>